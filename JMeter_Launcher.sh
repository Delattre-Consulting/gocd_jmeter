#!/bin/bash

###
# Function declarations
###

# The default pass for the installation of JMeter
PATH_JMETER="/usr/share/jmeter/bin/jmeter"

# Test the params givent to the script when started
test_init_params() {

	echo "Test params:"

	# Is JMeter available?
	if [ -f $PATH_JMETER ]
	then
		echo "JMeter available."
	else
		echo "ERROR - JMeter wasn't found on the system. Expected location:" $PATH_JMETER
		return 100
	fi

	# Is Test file available?
	if [ -f $1 ]
	then
		echo "Test file available: " $1
	else
		echo "ERROR - Can't find the Test file " $1
		return 101
	fi

	# Is log name not empty
	if [ ! -z "$2" -a "$2" != " " ]
	then
		echo "Log name not blank: " $2
	else
		echo "WARNING - Log name is empty!"
	fi

	echo "Params OK!"
	return 0
}

# Init the value fo the variable LOG_FILE_NAME with the log file name givent to the script
# and the date and time of execution.
# Add the "log" extension to the log file.
time_logfile() {
	echo "-----------------------------"
	echo "Generate a name with the execution time for the log file..."

	# Date and Time at the execution
	today=`date +%Y%m%d_%H%M%S`
	
	LOG_FILE_NAME="$1_$today.log"
	echo "Name for the log file : " $LOG_FILE_NAME
}

# Launch the jmeter command
launch_jmeter() {
	echo "-----------------------------"
	echo "Launch JMeter:"

	#Params details:
	# PATH_JMETER : map the JMeter cmd
	# -n : disable the UI, allow using jmeter without UI
	# -t $1 : the test to execute, $1 contains the path to the test
	# -l $2 : the log file, $2 contains the path to the log file
	eval $PATH_JMETER -n -t $1 -l $2
	return 0
}

# Alanyse the result of the JMeter script: look at the log file to count how many "OK" there are.
# Compare the number of "OK" to the number of line. If some line don't contain "OK", it means
# there was at least one error.
result_analysis() {
	echo "-----------------------------"
	echo "Result analysis:"

	# Was the log file created?
	if [ -f $1 ] 
	then
		echo "Log file was created, can be interpreted (" $1 ")"
	else
		echo "ERROR - No log file created"
		return 300
	fi

	# count NB lines in total
	NB_LINES_TOTAL="$(grep -w "" -c $1)"
	echo "Number of lines in log file =" $NB_LINES_TOTAL

	# count NB lines OK
	PATTERN="OK"
	NB_LINES_OK="$(grep -w "$PATTERN" -c $1)"
	echo "Number of lines which are OK =" $NB_LINES_OK

	# Compare Nb lines OK vs Nb lines in total
	if [[ "$NB_LINES_TOTAL" > "$NB_LINES_OK" ]]
	then
		echo "ERROR - Some tests failed! See the log:" $1
		return 301
	else
		echo "OK - All the results are OK"
	fi
}


### 
# Starts the script
###
echo "GoCD-JMeter launcher - START"
echo "--------------------------------------------------"

# Test the params given to launch JMeter
test_init_params $1 $2
TEST_PARAMS=$?
if [[ "$TEST_PARAMS" -ge "1" ]]
then
	exit 1
fi

# Add the exec time to the name of the log file
LOG_FILE_NAME="NO_NAME.log"
time_logfile $2

# Launch JMeter
launch_jmeter $1 $LOG_FILE_NAME

# Analyse the result whicjh is in a file
result_analysis $LOG_FILE_NAME
RESULT_ANALYSE=$?
# Test error on the result
if [[ "$RESULT_ANALYSE" -ge "1" ]] 
then
	# One error in the JMeter result, return an error to GoCD
	exit 1
fi

echo "--------------------------------------------------"
echo "GoCD-JMeter launcher - END"
